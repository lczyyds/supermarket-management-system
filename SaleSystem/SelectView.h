/* * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Filename: Selectiew.h
 * Author:   Student @B站up摸鱼的喵
 * Date:     2020年11月28日
 *
 * Description:
 *			有关Selectiew模块的声明
 *
 * Modified:
 *	2020-11-29 Student @B站up摸鱼的喵 树视图功能的声明
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#pragma once


// CSelectView 视图

class CSelectView : public CTreeView
{
	DECLARE_DYNCREATE(CSelectView)

protected:
	CSelectView();           // 动态创建所使用的受保护的构造函数
	virtual ~CSelectView();

public:
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	DECLARE_MESSAGE_MAP()
private:
	CTreeCtrl *m_treeCtrl;	//树控件
	CImageList m_imageList;	//图标列表

public:
	virtual void OnInitialUpdate();
	afx_msg void OnTvnSelchanged(NMHDR *pNMHDR, LRESULT *pResult);
};


