﻿// : 实现文件
//

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Filename:CAddUser.cpp
 * Author:   Student @B站up摸鱼的喵
 * Date:     2020年12月10日
 *
 * Description:
 *				编写关于用户添加的函数
 *
 * Modified:
 *
* 
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * */



#include "stdafx.h"
#include "SaleSystem.h"
#include "CAddUser.h"
#include "InfoFile.h"

// CAddUser

IMPLEMENT_DYNCREATE(CAddUser, CFormView)

CAddUser::CAddUser()
	: CFormView(DIALOG_USER_ADD)
	, m_name(_T(""))
	, m_pwd(_T(""))
	, control(0)
{

}

CAddUser::~CAddUser()
{
}

void CAddUser::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT3, m_name);
	DDX_Text(pDX, IDC_EDIT4, m_pwd);
	DDX_Text(pDX, IDC_EDIT8, control);
}

BEGIN_MESSAGE_MAP(CAddUser, CFormView)
	ON_BN_CLICKED(IDC_BUTTON1, &CAddUser::OnBnClickedButton1)
END_MESSAGE_MAP()


// CAddUser 诊断

#ifdef _DEBUG
void CAddUser::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CAddUser::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CAddUser 消息处理程序


void CAddUser::OnBnClickedButton1()
{
	// TODO: 在此添加控件通知处理程序代码

	UpdateData(TRUE); //获取控件内容

	if (!(control == 2 || control == 1) || m_pwd.IsEmpty()|| m_name.IsEmpty())
	{
		MessageBox(TEXT("输入信息有误"));
		return;
	}
	CInfoFile file;
	file.ReadUserline(); //读取用户信息
	file.AddUserline(m_name, m_pwd, control); //添加用户
	file.WirteUserline(); //写文件
	file.l_ls.clear(); //清空list的内容
	MessageBox(_T("添加成功"));

	m_name.Empty();
	m_pwd.Empty();
	control = 0;
	UpdateData(FALSE);
}
