﻿#pragma once
#include "afxwin.h"
#include "SaleSystem.h"
#include "InfoFile.h"

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Filename:AddDlg.h
 * Author:   Student @B站up摸鱼的喵
 * Date:     2020年12月1日
 *
 * Description:
					用户添加的模块函数声明
 *
 * Modified:
 *
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * */




// CAddUser 窗体视图

class CAddUser : public CFormView
{
	DECLARE_DYNCREATE(CAddUser)

protected:
	CAddUser();           // 动态创建所使用的受保护的构造函数
	virtual ~CAddUser();

public:
#ifdef AFX_DESIGN_TIME
	enum { IDD = DIALOG_USER_ADD };
#endif
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
private:
	CString m_name;
	CString m_pwd;
	int control;
};


