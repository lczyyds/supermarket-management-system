﻿// CInfoUser.cpp: 实现文件
//

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Filename:CInfoUser.cpp
 * Author:   Student @B站up摸鱼的喵
 * Date:     2020年12月 12日
 *
 * Description:
 *				添加的显示用户模块
 *
 * Modified:
 *
 * 2020-   Student @B站up摸鱼的喵
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * */



#include "stdafx.h"
#include "SaleSystem.h"
#include "CInfoUser.h" 


// CInfoUser

IMPLEMENT_DYNCREATE(CInfoUser, CFormView)

CInfoUser::CInfoUser()
	: CFormView(DIALOG_USER_INFO)
{

}

CInfoUser::~CInfoUser()
{
}

void CInfoUser::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, u_list);
}

BEGIN_MESSAGE_MAP(CInfoUser, CFormView)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST1, &CInfoUser::OnLvnItemchangedList1)
END_MESSAGE_MAP()


// CInfoUser 诊断

#ifdef _DEBUG
void CInfoUser::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CInfoUser::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CInfoUser 消息处理程序


void CInfoUser::OnLvnItemchangedList1(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	*pResult = 0;
}


void CInfoUser::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	// TODO: 在此添加专用代码和/或调用基类

	u_list.SetExtendedStyle(u_list.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	// 初始化表头
	CString field[] = { _T("用户ID"), _T("用户名"), _T("密码"), _T("权限") };
	for (int i = 0; i < sizeof(field) / sizeof(field[0]); ++i)
	{
		u_list.InsertColumn(i, field[i], LVCFMT_CENTER, 90);
	}
	 
	CInfoFile file;
	file.ReadUserline(); //读取用户信息

	//添加数据
	int i = 0;
	CString str;
	for (list<l_user>::iterator it = file.l_ls.begin(); it != file.l_ls.end(); it++)
	{
		str.Format(_T("%d"), it->id);
		u_list.InsertItem(i, str);
		int column = 1;
		u_list.SetItemText(i, column++, (CString)it->name.c_str());
		str.Format(_T("%d"), it->name);
		u_list.SetItemText(i, column++, (CString)it->l_pwd.c_str());
		str.Format(_T("%d"), it->l_pwd);
		str.Format(_T("%d"), it->control);
		u_list.SetItemText(i, column++, str);
		i++;
	}


}
