﻿#pragma once
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Filename: DispalyView.h
 * Author:   Student @B站up摸鱼的喵
 * Date:     2020年11月28日
 *
 * Description:
 *				自定义MFC视图类
 *
 * Modified:
 *
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#include "afxwin.h"

#include "InfoFile.h"
#include "SaleSystem.h"


// CInfoUser 窗体视图

class CInfoUser : public CFormView
{
	DECLARE_DYNCREATE(CInfoUser)

protected:
	CInfoUser();           // 动态创建所使用的受保护的构造函数
	virtual ~CInfoUser();

public:
#ifdef AFX_DESIGN_TIME
	enum { IDD = DIALOG_USER_INFO };
#endif
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
private:
	CListCtrl u_list;
public:
	afx_msg void OnLvnItemchangedList1(NMHDR* pNMHDR, LRESULT* pResult);
	virtual void OnInitialUpdate();
};


