#pragma once

#include <list>
#include <fstream>
#include <iostream>
#include <string>


#define _F_LOGIN "d://Lcz//login.ini"
#define _F_STOCK "d://Lcz//stock.txt"
#define _F_USER "d://Lcz//user.txt"

using namespace std;

struct msg
{
	int id;				//商品id
	string name;	//商品名
	int price;			//商品价格
	int num;			//商品个数
};

struct l_user
{
	int id;				//商品id
	string name;	//商品名
	string l_pwd;			//商品个数
	int control;			//商品价格
};

class CInfoFile
{
public:
	CInfoFile();
	~CInfoFile();

	//读取登陆信息
	void ReadLogin(CString &name, CString &pwd);

	//修改密码
	void WritePwd(char* name, char* pwd);


	// 读取用户数据
	void ReadUserline();

	//用户写入文件
	void WirteUserline();

	//添加新用户
	void AddUserline(CString name, CString num, int price);

	// 读取商品数据
	void ReadDocline();

	//商品写入文件
	void WirteDocline();

	//添加新商品
	void Addline(CString name, int num, int price);

	list<msg> ls;	//存储商品容器
	int num;			//用来记录商品个数


	list<l_user> l_ls;
	int l_num;
};

