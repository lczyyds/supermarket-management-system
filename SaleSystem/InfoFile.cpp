#include "stdafx.h"
#include "InfoFile.h"

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Filename:InfoFile.cpp
 * Author:   Student @B站up摸鱼的喵
 * Date:     2020年 12月 3日
 *
 * Description:
 *				添加的对商品添加、删除、录入的函数
 *
 * Modified:
 *
 * 2020-12-12   Student @B站up摸鱼的喵
 *				添加的对用户添加、删除、录入的函数
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * */



CInfoFile::CInfoFile()
{
}


CInfoFile::~CInfoFile()
{
}

//读取登陆信息
void CInfoFile::ReadLogin(CString &name, CString &pwd)
{
	ifstream ifs; //创建文件输入对象
	ifs.open(_F_LOGIN); //打开文件

	char buf[1024] = { 0 };

	ifs.getline(buf, sizeof(buf)); //读取一行内容
	name = CString(buf);			 //char *转换为CString

	ifs.getline(buf, sizeof(buf));
	pwd = CString(buf);

	ifs.close(); //关闭文件
}

//修改密码
void CInfoFile::WritePwd(char* name, char* pwd)
{

	
}
//读取用户信息
void CInfoFile::ReadUserline()
{
	ifstream ifs(_F_USER); //输入方式打开文件

	char buf[1024] = { 0 };
	l_num = 0;	//初始化用户数目为0
	l_ls.clear();
	//取出表头
	ifs.getline(buf, sizeof(buf));

	while (!ifs.eof()) //没到文件结尾
	{
		l_user l_tmp;

		ifs.getline(buf, sizeof(buf)); //读取一行
		l_num++;	//用户数目加一

		//AfxMessageBox(CString(buf));
		char* sst = strtok(buf, "|"); //以“|”切割
		if (sst != NULL)
		{
			l_tmp.id = atoi(sst); //商品用户编号
		}
		else
		{
			break;
		}

		sst = strtok(NULL, "|");
		l_tmp.name = sst;	//用户名

		sst = strtok(NULL, "|");
		l_tmp.l_pwd = sst;	//用户密码

		sst = strtok(NULL, "|");
		l_tmp.control = atoi(sst);	//用户权限

		l_ls.push_back(l_tmp); //放在链表的后面
	}

	ifs.close(); //关闭文件
}

//用户写入文件
void CInfoFile::WirteUserline()
{
	ofstream ofs(_F_USER);//输出方式打开文件

	if (l_ls.size() > 0)	//用户链表有内容才执行
	{
		ofs << "用户ID|用户名|密码|权限" << endl; //写入表头

		//通过迭代器取出链表内容，写入文件，以“|”分隔，结尾加换行
		for (list<l_user>::iterator it = l_ls.begin(); it != l_ls.end(); it++)
		{
			ofs << it->id << "|";
			ofs << it->name << "|";
			ofs << it->l_pwd << "|";
			ofs << it-> control << endl;
		}
	}

	ofs.close();//关闭文件
}

//添加新用户
//name:用户名名称，l_pwd：密码，control：权限
void CInfoFile::AddUserline(CString name, CString l_pwd, int control)
{
	l_user l_tmp;

	if (l_ls.size() > 0)
	{
		//用户名，密码，权限
		if (!name.IsEmpty() && !l_pwd.IsEmpty() > 0 && control > 0)
		{
			l_tmp.id = l_ls.size() + 1;	//id自动加1
			CStringA str;
			str = name;	//CString转CStirngA

			CStringA str2;
			str2 = l_pwd;	//CString转CStirngA
			l_tmp.name = str.GetBuffer(); 
			l_tmp.l_pwd = str2.GetBuffer();	
			l_tmp.control = control;	

			l_ls.push_back(l_tmp);	//放在链表的后面
		}
	}
}

//读取商品信息
void CInfoFile::ReadDocline()
{
	ifstream ifs(_F_STOCK); //输入方式打开文件

	char buf[1024] = { 0 };
	num = 0;	//初始化商品数目为0
	ls.clear();
	//取出表头
	ifs.getline(buf, sizeof(buf));

	while (!ifs.eof()) //没到文件结尾
	{
		msg tmp;

		ifs.getline(buf, sizeof(buf)); //读取一行
		num++;	//商品数目加一

		//AfxMessageBox(CString(buf));
		char *sst = strtok(buf, "|"); //以“|”切割
		if (sst != NULL)
		{
			tmp.id = atoi(sst); //商品id
		}
		else
		{
			break;
		}

		sst = strtok(NULL, "|");
		tmp.name = sst;	//商品名称

		sst = strtok(NULL, "|");
		tmp.price = atoi(sst);	//商品价格

		sst = strtok(NULL, "|");
		tmp.num = atoi(sst);	//商品数目

		ls.push_back(tmp); //放在链表的后面
	}

	ifs.close(); //关闭文件
}

//商品写入文件
void CInfoFile::WirteDocline()
{
	ofstream ofs(_F_STOCK);//输出方式打开文件

	if (ls.size() > 0)	//商品链表有内容才执行
	{
		ofs << "商品ID|商品名称|商品价格|库存" << endl; //写入表头

		//通过迭代器取出链表内容，写入文件，以“|”分隔，结尾加换行
		for (list<msg>::iterator it = ls.begin(); it != ls.end(); it++)
		{
			ofs << it->id << "|";		
			ofs << it->name << "|";
			ofs << it->price << "|";
			ofs << it->num << endl;
		}
	}

	ofs.close();//关闭文件
}

//添加新商品
//name:商品名称，num：库存，price：价格
void CInfoFile::Addline(CString name, int num, int price)
{
	msg tmp;

	if (ls.size() > 0)
	{
		//商品名称，库存，价格有效
		if (!name.IsEmpty() && num > 0 && price > 0)
		{
			tmp.id = ls.size() + 1;	//id自动加1
			CStringA str;
			str = name;	//CString转CStirngA
			tmp.name = str.GetBuffer(); //CStirngA转char *，商品名称
			tmp.num = num;	//库存
			tmp.price = price;	//价格

			ls.push_back(tmp);	//放在链表的后面
		}
	}
}
