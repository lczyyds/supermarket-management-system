// LoginDlg.cpp : 实现文件
//



/* * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Filename:LoginDlg.cpp
 * Author:   Student @B站up摸鱼的喵
 * Date:     2020年 月 日
 *
 * Description:
 *				登录
 *
 * Modified:
 *
 * 2020-   Student @B站up摸鱼的喵
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "stdafx.h"
#include "SaleSystem.h"
#include "LoginDlg.h"
#include "afxdialogex.h"
#include "InfoFile.h"

#include"JudgeUser.h"

// CLoginDlg 对话框

IMPLEMENT_DYNAMIC(CLoginDlg, CDialogEx)

CLoginDlg::CLoginDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CLoginDlg::IDD, pParent)
	, m_user(_T(""))
	, m_pwd(_T(""))
{

}

CLoginDlg::~CLoginDlg()
{
}

void CLoginDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_user);
	DDX_Text(pDX, IDC_EDIT2, m_pwd);
}


BEGIN_MESSAGE_MAP(CLoginDlg, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON1, &CLoginDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CLoginDlg::OnBnClickedButton2)
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CLoginDlg 消息处理程序


void CLoginDlg::OnBnClickedButton1()
{
	// TODO:  在此添加控件通知处理程序代码
	//登陆按钮点击事件

	UpdateData(TRUE); 

	if (m_user.IsEmpty() || m_pwd.IsEmpty())
	{
		MessageBox(TEXT("输入内容不能为空"));
		return;
	}


	CInfoFile file;
	file.ReadUserline(); //读取用户信息
	int falg = 0;
	//std::string L_m_pwd(CW2A(m_pwd.GetString()));
	//std::string L_m_user(CW2A(m_user.GetString()));
	for (list<l_user>::iterator it = file.l_ls.begin(); it != file.l_ls.end(); it++)
	{
		CString cstr_name(it->name.c_str());
		CString cstr_pwd(it->l_pwd.c_str());
		if ((cstr_name == m_user)&& (cstr_pwd == m_pwd ))
		{

			if (it->control != 0)
			{
				falg =1;
				set_judge(it->control, cstr_name);
				break;
			}
			else
			{
				falg = 2;
				break;
			}
		}
		else
		{
			falg = 0;
		}
		//m_combo.AddString((CString)it->name.c_str());

	}
	if (falg == 1)
	{
		//关闭当前对话框
		CDialog::OnCancel();
	}
	else if (falg == 2)
	{
		MessageBox(TEXT("该用户已注销"));
	}
	else
	{ 
		MessageBox(TEXT("用户名或密码不存在"));
	}

	////获取正确的值
	//CInfoFile file;
	//CString name, pwd;
	//file.ReadLogin(name, pwd);

	//if (name == m_user) //用户名一致
	//{
	//	if (pwd == m_pwd) //密码一致
	//	{
	//		//关闭当前对话框
	//		CDialog::OnCancel();
	//	}
	//	else
	//	{
	//		MessageBox(TEXT("密码有误"));
	//	}
	//}
	//else
	//{
	//	MessageBox(TEXT("用户名有误"));
	//}
}


BOOL CLoginDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化

	// 默认登陆信息
	CInfoFile file;
	CString name, pwd;
	file.ReadLogin(name, pwd);

	m_user = name;
	m_pwd = pwd;
	//同步到控件中
	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常:  OCX 属性页应返回 FALSE
}


void CLoginDlg::OnBnClickedButton2()
{
	// TODO:  在此添加控件通知处理程序代码
	exit(0);
}


void CLoginDlg::OnOK()
{
	// TODO:  在此添加专用代码和/或调用基类

	//CDialogEx::OnOK();
}


void CLoginDlg::OnClose()
{
	// TODO:  在此添加消息处理程序代码和/或调用默认值
	//关闭当前对话框
	//CDialogEx::OnClose();

	//退出程序
	exit(0);
}
