﻿/* * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Filename: CInfoDlg.h
 * Author:   Student @B站up摸鱼的喵
 * Date:     2020年11月26日
 *
 * Description:
 *				添加关于文件读取操作的函数
 * 
 * Modified:
 *		 2020-11-27  Student @B站up摸鱼的喵 登录相关函数的声明
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#pragma once
#include "afxwin.h"

#include "InfoFile.h"
#include "SaleSystem.h"


// CInfoDlg 窗体视图

class CInfoDlg : public CFormView
{
	DECLARE_DYNCREATE(CInfoDlg)

protected:
	CInfoDlg();           // 动态创建所使用的受保护的构造函数
	virtual ~CInfoDlg();

public:
#ifdef AFX_DESIGN_TIME
	enum { IDD = DIALOG_INFO };
#endif
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
private:
	CListCtrl m_list;
public:
	virtual void OnInitialUpdate();
};


