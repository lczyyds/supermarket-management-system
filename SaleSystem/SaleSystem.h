
// SaleSystem.h : SaleSystem 应用程序的主头文件
//

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Filename:SaleSystem.h
 * Author:   Student @B站up摸鱼的喵
 * Date:     2020年11月26日
 *
 * Description:
 *				初始化窗口的相关声明
 *
 * Modified:
 *
 * 2020-11-29   Student @B站up摸鱼的喵		修改代码
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#pragma once

#ifndef __AFXWIN_H__
	#error "在包含此文件之前包含“stdafx.h”以生成 PCH 文件"
#endif

#include "resource.h"       // 主符号


// CSaleSystemApp:
// 有关此类的实现，请参阅 SaleSystem.cpp
//

class CSaleSystemApp : public CWinApp
{
public:
	CSaleSystemApp();


// 重写
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

// 实现
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CSaleSystemApp theApp;
