﻿// CDelUser.cpp: 实现文件
//

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Filename: CInfoDlg.h
 * Author:   Student @B站up摸鱼的喵
 * Date:     2020年12月12日
 *
 * Description:
 *				添加关于删除用户模块			
 * 
 * Modified:
 *
 * 2020-12-27  Student @B站up摸鱼的喵  登录功能
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#include "stdafx.h"
#include "SaleSystem.h"
#include "CDelUser.h"
#include "InfoFile.h"


// CDelUser

IMPLEMENT_DYNCREATE(CDelUser, CFormView)

CDelUser::CDelUser()
	: CFormView(DIALOG_USER_DEL)
	, m_pwd(_T(""))
	, control(0)
{

}

CDelUser::~CDelUser()
{
}

void CDelUser::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO1, m_combo);
	DDX_Text(pDX, IDC_EDIT1, m_pwd);
	DDX_Text(pDX, IDC_EDIT2, control);
}

BEGIN_MESSAGE_MAP(CDelUser, CFormView)
	ON_CBN_SELCHANGE(IDC_COMBO1, &CDelUser::OnCbnSelchangeCombo1)
	ON_BN_CLICKED(IDC_BUTTON1, &CDelUser::OnBnClickedButton1)
END_MESSAGE_MAP()


// CDelUser 诊断

#ifdef _DEBUG
void CDelUser::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CDelUser::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CDelUser 消息处理程序


void CDelUser::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	//读取文件，获取用户名，给组合框添加字符串
	CInfoFile file;
	file.ReadUserline(); //读取用户信息
	for (list<l_user>::iterator it = file.l_ls.begin(); it != file.l_ls.end(); it++)
	{
		m_combo.AddString((CString)it->name.c_str());
	}


	//将第一个用户名设为默认选中项
	m_combo.SetCurSel(0);
}


void CDelUser::OnCbnSelchangeCombo1()
{
	// TODO: 在此添加控件通知处理程序代码

	CString text;
	//获取当前选中项
	int index = m_combo.GetCurSel();
	//获取当前内容
	m_combo.GetLBText(index, text);
	 
	CInfoFile file;
	file.ReadUserline(); //读取用户信息
	for (list<l_user>::iterator it = file.l_ls.begin(); it != file.l_ls.end(); it++)
	{
		if (text == it->name.c_str())
		{
			m_pwd = it->l_pwd.c_str();
			control = it->control;
			UpdateData(FALSE); //内容更新到对应的控件
		}
	}
}


void CDelUser::OnBnClickedButton1()
{
	// TODO: 在此添加控件通知处理程序代码


	//获取控件上的内容，更新到对应关联的变量中
	UpdateData(TRUE);


	CString type;
	//获取当前选中项
	int index = m_combo.GetCurSel();
	//获取组合框当前内容
	m_combo.GetLBText(index, type);

	CString str;
	str.Format(_T("注销用户：%s \r\n密码：%s \r\n权限：%d "), type, m_pwd, control);
	MessageBox(str);

	 
	CInfoFile file;
	file.ReadUserline(); //读取用户信息
	for (list<l_user>::iterator it = file.l_ls.begin(); it != file.l_ls.end(); it++)
	{
		if (type == it->name.c_str())
		{
			//it->control = it->control - control;
			/*it->name.Empty();

			m_name.Empty();*/
			it->control = 0;
		}
	}
	file.WirteUserline(); //更新文件内容



	UpdateData(FALSE); //更新到对应的控件
}
