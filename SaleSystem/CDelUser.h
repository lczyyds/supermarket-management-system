﻿#pragma once

#include "afxwin.h"

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Filename: CInfoDlg.h
 * Author:   Student @B站up摸鱼的喵
 * Date:     2020年12月12日
 *
 * Description:
 *				删除用户模块相关声明
 *
 * Modified:
 *
 * 2020-12-27  Student @B站up摸鱼的喵  登录功能
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// CDelUser 窗体视图

class CDelUser : public CFormView
{
	DECLARE_DYNCREATE(CDelUser)

protected:
	CDelUser();           // 动态创建所使用的受保护的构造函数
	virtual ~CDelUser();

public:
#ifdef AFX_DESIGN_TIME
	enum { IDD = DIALOG_USER_DEL };
#endif
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
private:
	CComboBox m_combo;
	CString m_pwd;
	int control;
public:
	virtual void OnInitialUpdate();
	afx_msg void OnCbnSelchangeCombo1();
	afx_msg void OnBnClickedButton1();
};


