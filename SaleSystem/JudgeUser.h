#pragma once

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Filename:JudgeUser.h
 * Author:   Student @B站up摸鱼的喵
 * Date:     2020年12月21日
 *
 * Description:
 *				JudgeUser函数相关声明
 *
 * Modified:
 *
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

void set_judge(int num, CString name);


int out_judge();


CString out_cstr_name();